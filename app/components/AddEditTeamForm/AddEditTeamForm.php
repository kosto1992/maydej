<?php
/**
 * Add/edit team form component
 * @author Jakub Hadamčík <jakub@hadamcik.cz>
 */

namespace App\Components;

use App\Model\SportsRepository;
use App\Model\TeamsRepository;
use Nette\Application\UI;

/**
 * Class AddEditTeamForm
 * @package App\Components
 */
class AddEditTeamForm extends UI\Control
{
    use TemplateTrait;

    /** @var \App\Model\TeamsRepository */
    private $teams;

    /** @var \App\Model\SportsRepository */
    private $sports;

    /** @var int */
    private $id = NULL;

    public function __construct(TeamsRepository $teams, SportsRepository $sports) {
        $this->teams = $teams;
        $this->sports = $sports;
    }

    protected function createComponentForm() {
        $form = new UI\Form();

        $form->setTranslator($this->presenter->translator);

        $team = $this->teams->get($this->id);

        $form->addSelect('sport', 'components.addEditTeamForm.sport.title')
            ->setItems($this->sports->getAll()->fetchPairs('id', 'title'))
            ->setPrompt('components.addEditTeamForm.sport.prompt')
            ->setRequired('components.addEditTeamForm.sport.required');

        $form->addText('name', 'components.addEditTeamForm.name.title')
            ->setRequired('components.addEditTeamForm.name.required');

        $form->addText('email', 'components.addEditTeamForm.email.title')
            ->setRequired('components.addEditTeamForm.email.required')
            ->addRule(UI\Form::EMAIL, 'components.addEditTeamForm.email.valid');

        if($this->id == NULL) {
            $form->addPassword('password', 'components.addEditTeamForm.password.title')
                ->setRequired('components.addEditTeamForm.password.required');

            $form->addPassword('password2', 'components.addEditTeamForm.password2.title')
                ->setRequired('components.addEditTeamForm.password2.required')
                ->addRule(UI\Form::EQUAL, 'components.addEditTeamForm.password2.equal', $form['password']);
        }

        $form->addText('leader', 'components.addEditTeamForm.leader.title')
            ->setRequired('components.addEditTeamForm.leader.required');

        $form->addText('referee', 'components.addEditTeamForm.referee.title')
            ->setRequired('components.addEditTeamForm.referee.required');

        $form->addTextArea('note', 'components.addEditTeamForm.note.title');

        $form->addSubmit('submit', ($team == FALSE ? 'components.addEditTeamForm.submit.add' : 'components.addEditTeamForm.submit.edit'));

        if($team != FALSE) {
            $form->setDefaults(array(
                'sport' => $team->sport_id,
                'email' => $team->email,
                'name' => $team->name,
                'leader' => $team->leader,
                'referee' => $team->referee,
                'note' => $team->note
            ));
        }

        $form->onValidate[] = $this->validateForm;
        $form->onSuccess[] = $this->processForm;

        return $form;
    }

    public function validateForm(UI\Form $form) {
        $values = $form->getValues();
        $user = $this->teams->getByEmail($values->email);

        if($user != FALSE && $user->id != $this->id) {
            $form->addError('components.addEditTeamForm.email.exists');
        }
    }

    public function processForm(UI\Form $form) {
        $values = $form->getValues();

        if($this->id == NULL) {
            $this->teams->add($values->name, $values->email, $values->password, $values->sport, $values->leader, $values->referee, $values->note);
            $this->presenter->flashMessage('components.addEditTeamForm.flashes.added');
            $this->presenter->redirect(':Sign:in');
        }
        else {
            $this->teams->edit($this->id, $values->name, $values->email, $values->sport, $values->leader, $values->referee, $values->note);
            $this->presenter->flashMessage('components.addEditTeamForm.flashes.edited');
            $this->presenter->redirect(':Team:settings');
        }
    }

    public function setId($id) {
        $this->id = $id;
        return $this;
    }
}

/**
 * Interface IAddEditTeamFormFactory
 * @package App\Components
 */
interface IAddEditTeamFormFactory
{
    /**
     * Creates sign in form component
     * @return \App\Components\AddEditTeamForm
     */
    function create();
}
