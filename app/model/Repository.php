<?php
/**
 * Repository
 * @author Václav Ostrožlík <vaclav@ostrozlik.cz>
 */

namespace App\Model;

use Nette;
use Nette\Database;

/**
 * Class Repository
 * @package App\Model
 */
class Repository extends Nette\Object
{

	/**
     * Contains database context connection
     * @var \Nette\Database\Context
     */
	protected $context;

    /**
     * Contains table name
     * @var string
     */
    protected $table = NULL;

	/**
     * Constructs repository class
	 * @param \Nette\Database\Context
	 */
	public function __construct(Database\Context $context)
	{
		$this->context = $context;
	}

    /**
     * Returns table name
     * @return string
     */
    public function getTableName() {
        preg_match('#(\w+)Repository$#', get_class($this), $m);
        return ($this->table !== NULL ? $this->table : lcfirst($m[1]));
    }

	/**
	 * Returns all items from database
	 * @return \Nette\Database\Table\Selection
	 */
	public function getTable()
	{
		return $this->context->table($this->getTableName());
	}

	/**
	 * Finds one item by array
	 * @param array $conds
	 * @return \Nette\Database\Table\ActiveRow|FALSE
	 */
	public function getOne(array $conds)
	{
		return $this->getAll($conds)->limit(1)->fetch();
	}

	/**
	 * Finds item by primary key
	 * @param int $id
	 * @return \Nette\Database\Table\ActiveRow|FALSE
	 */
	public function get($id)
	{
		return $this->getTable()->get($id);
	}

	/**
	 * Finds items by filter
	 * @param array $filters
	 * @return \Nette\Database\Table\Selection
	 */
	public function getAll(array $filters = array())
	{
		return $this->getTable()->where($filters);
	}

}
