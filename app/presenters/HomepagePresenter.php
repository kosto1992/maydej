<?php

namespace App\Presenters;

use Nette,
	App\Model;


/**
 * Homepage presenter.
 */
class HomepagePresenter extends BasePresenter
{
    /** @var Model\SportsRepository @inject */
    public $sports;

    public function actionDefault() {
        $this->template->setFile(__DIR__ . '/templates/Homepage/default/' . $this->translator->getLocale() . '.latte');
    }

    public function actionTeams() {
        $this->template->sports = $this->sports->getAll();
    }
}
