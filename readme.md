Maydej website
==============

This repository is made for store changes on http://maydej.buk.cvut.cz , www.maydej.cz , http://maydej.cz website. This website is based on Nette Sandbox, viz official guide below, BUT official guide won't work to setup this website, because it is written for older versions than are available. The installation of project in official guide will use too new versions and simply it is not compatible.

Installation guide
------------------
- Clone repository from :

        git clone https://bitbucket.org/kosto1992/maydej.git
        cd maydej

- Next use already installed composer (according to official guide) & compose project:

    `composer update`

    Notice.: This command creates files in `vendor` directory

- If it hasn't been already created, create directories in `maydej` directory:

    `mkdir temp log`

- Next this app works with database on localhost so to work properly you have to have set up MySQL database with exactly same scheme as provided. To sum up we expect you have **working LAMP** installation with **correct database scheme**. To access local database there is a file `/var/www/maydej/app/config/config.local.neon.example`,
where are stored credentials to MySQL database. To have a template it is enclosed in this repository, but BLANK. So you have to insert your credentials to make it working properly and at the end copy it like:

    `cp config.local.neon.example config.local.neon`

- To be sure all files are accessible (except forbidden files secured with .htaccess files) change owner:

    `chown -R www-data:www-data maydej`


Update guide
------------
- If you have already created this project and want to update the information, update via git (i.e. via git from origin to master branch):

    `git pull origin master`

- To uncache old version delete cache directory in temp directory:

    `rm -r \var\www\maydej\temp\cache`

    Notice.: It is important to finish update, or you will see old website despite having all files updated.

- Check updates :-)


Nette Sandbox
=============

This is a simple pre-packaged and pre-configured application using the [Nette](https://nette.org)
that you can use as the starting point for your new applications.

[Nette](https://nette.org) is a popular tool for PHP web development.
It is designed to be the most usable and friendliest as possible. It focuses
on security and performance and is definitely one of the safest PHP frameworks.


Installation
------------

The best way to install Web Project is using Composer. If you don't have Composer yet,
download it following [the instructions](https://doc.nette.org/composer). Then use command:

	composer create-project nette/sandbox path/to/install
	cd path/to/install


Make directories `temp/` and `log/` writable.


Web Server Setup
----------------

The simplest way to get started is to start the built-in PHP server in the root directory of your project:

	php -S localhost:8000 -t www

Then visit `http://localhost:8000` in your browser to see the welcome page.

For Apache or Nginx, setup a virtual host to point to the `www/` directory of the project and you
should be ready to go.

It is CRITICAL that whole `app/`, `log/` and `temp/` directories are not accessible directly
via a web browser. See [security warning](https://nette.org/security-warning).


Requirements
------------

PHP 5.6 or higher. To check whether server configuration meets the minimum requirements for
Nette Framework browse to the directory `/checker` in your project root (i.e. `http://localhost:8000/checker`).


Adminer
-------

[Adminer](https://www.adminer.org/) is full-featured database management tool written in PHP and it is part of this Sandbox.
To use it, browse to the subdirectory `/adminer` in your project root (i.e. `http://localhost:8000/adminer`).


License
-------
- Nette: New BSD License or GPL 2.0 or 3.0
- Adminer: Apache License 2.0 or GPL 2