<?php

namespace App\Presenters;

use Nette,
	App\Model;


/**
 * Sign presenter.
 */
class SignPresenter extends BasePresenter
{
    /** @var \App\Components\IAddEditTeamFormFactory @inject */
    public $addEditTeamForm;

    /** @var \App\Components\ISignInFormFactory @inject */
    public $signInForm;

    public function actionUp() {
//        $this->flashMessage('sign.up.late', 'error');
//        $this->redirect(':Homepage:default');
        $this->template->setFile(__DIR__ . '/templates/Sign/up/' . $this->translator->getLocale() . '.latte');
    }

    public function actionOut() {
        $this->user->logout(TRUE);
        $this->flashMessage('sign.out.flashes.success');
        $this->redirect(':Homepage:default');
    }

    protected function createComponentUpForm() {
        return $this->addEditTeamForm->create();
    }

    protected function createComponentInForm() {
        return $this->signInForm->create();
    }
}
