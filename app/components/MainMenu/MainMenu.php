<?php
/**
 * Main menu component
 * @author Jakub Hadamčík <jakub@hadamcik.cz>
 */

namespace App\Components;

use Nette\Application\UI;

/**
 * Class MainMenu
 * @package App\Components
 */
class MainMenu extends UI\Control
{
    use TemplateTrait;

    private function getMenu() {
        $menu = array();

        $informations = array(
            'link' => $this->presenter->link(':Homepage:default'),
            'title' => 'components.mainMenu.informations.title',
            'label' => 'components.mainMenu.informations.label'
        );
        $gallery = array(
            'link' => 'http://fotogalerie.maydej.cz',
            'title' => 'components.mainMenu.gallery.title',
            'label' => 'components.mainMenu.gallery.label'
        );
        $signUp = array(
            'link' => $this->presenter->link(':Sign:up'),
            'title' => 'components.mainMenu.signUp.title',
            'label' => 'components.mainMenu.signUp.label'
        );
        $signOut = array(
            'link' => $this->presenter->link(':Sign:out'),
            'title' => 'components.mainMenu.signOut.title',
            'label' => 'components.mainMenu.signOut.label'
        );

        if($this->presenter->isLinkCurrent(':Homepage:default')) {
            $menu[] = $gallery;
            $menu[] = ($this->presenter->user->isLoggedIn() ? $signOut : $signUp);
        }
        else if($this->presenter->isLinkCurrent(':Sign:*')) {
            $menu[] = $informations;
            $menu[] = $gallery;
        }
        else if($this->presenter->isLinkCurrent(':Homepage:teams')) {
            $menu[] = $informations;
            $menu[] = ($this->presenter->user->isLoggedIn() ? $signOut : $signUp);
        }
        else if($this->presenter->isLinkCurrent(':Team:settings')) {
            $menu[] = $informations;
            $menu[] = $gallery;
        }

        return $menu;
    }

    public function render() {
        $this->template->menu = $this->getMenu();
        $this->template->render();
    }
}

/**
 * Interface IMainMenuFactory
 * @package App\Components
 */
interface IMainMenuFactory
{
    /**
     * Creates sign in form component
     * @return \App\Components\MainMenu
     */
    function create();
}
