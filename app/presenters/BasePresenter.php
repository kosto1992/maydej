<?php

namespace App\Presenters;

use App\Components\IMainMenuFactory;
use Nette,
	App\Model;


/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter
{
    /** @var \Kdyby\Translation\Translator @inject */
    public $translator;

    /** @persistent */
    public $locale = 'cs';

    /** @var IMainMenuFactory @inject */
    public $mainMenu;

    /**
     * Creates template with translator
     * @param string
     * @return \Nette\Templating\ITemplate
     */
    protected function createTemplate($class = NULL) {
        $this->translator->setLocale($this->locale);
        $template = parent::createTemplate($class);
        $template->getLatte()->addFilter(NULL, callback($this->translator->createTemplateHelpers(), 'loader'));

        return $template;
    }

    protected function createComponentMainMenu() {
        return $this->mainMenu->create();
    }

}
