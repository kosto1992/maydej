Pre-requirements:

- Ansible installed locally.
- Password-less (key based) SSH access to `deploy@target-machine.com`.
- A `deploy` user on the `target-machine.com` having `sudo` access without password.

Steps to deploy the site:

1. Edit the `hosts` accordingly.
2. Install the roles by running: `ansible-galaxy install -r requirements.yml`.
3. If you re-deploy to existing machine, you must have `credentials/` files (see `vars.yml` for details).
4. Deploy by running: `ansible-playbook main.yml`.

Only the main site is fully deployed. The gallery has only Apache config + directory set up, you have to do the rest manually.
