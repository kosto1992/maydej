<?php
/**
 * Template trait
 * @author Jakub Hadamčík <jakub@hadamcik.cz>
 */

namespace App\Components;

/**
 * Class TemplateTrait
 * @package App\Components
 */
trait TemplateTrait
{
    /**
     * Creates component template with translator
     * @param string
     * @return \Nette\Templating\ITemplate
     */
    protected function createTemplate($class = NULL)
    {
        $template = parent::createTemplate($class);
        // change file extension to .latte
        $template->setFile(preg_replace('/\.[^.]+$/','.latte',$this->getReflection()->fileName));
        $template->getLatte()->addFilter(NULL, callback($this->presenter->translator->createTemplateHelpers(), 'loader'));

        return $template;
    }

    /**
     * Renders component template
     */
    public function render()
    {
        $this->template->render();
    }
} 